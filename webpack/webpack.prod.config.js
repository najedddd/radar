const _ = require('lodash')
const Webpack = require('webpack')

/**
 * Production build config.
 */
module.exports = _.assign({}, require('./webpack.common'), {
    plugins: [
        new Webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new Webpack.optimize.DedupePlugin(),
        new Webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })
    ]
})
