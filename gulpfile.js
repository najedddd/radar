/* eslint-disable no-console */
const gulp = require('gulp')
const gutil = require('gulp-util')
const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const devConfig = require('./webpack/webpack.dev.config')
const prodConfig = require('./webpack/webpack.prod.config')

gulp.task('develop', ['webpack:server'])
gulp.task('build:prod', ['webpack:prod'])

function webpackError(callback) {
    return (err, stats) => {
        if (err) {
            throw new gutil.PluginError('webpack', err)
        }
        gutil.log('[webpack]', stats.toString())
        callback()
    }
}

function webpackProgress(compiler) {
    let bundleStart
    compiler.plugin('compile', () => {
        console.log('Bundling...')
        bundleStart = Date.now()
    })

    compiler.plugin('done', () => {
        console.log(`Bundled in ${(Date.now() - bundleStart)}ms!`)
    })
}

gulp.task('webpack:server', (callback) => {
    const compiler = webpack(devConfig, webpackError(callback))

    webpackProgress(compiler)

    const server = new WebpackDevServer(compiler, {
        publicPath: '/js/',
        hot: true,
        quiet: false,
        noInfo: true,
        stats: {
            colors: true
        }
    })

    server.listen(9090, 'localhost', () => {
        console.log('Bundling project, please wait...')
    })
})

gulp.task('webpack:prod', (callback) => {
    webpack(prodConfig, webpackError(callback))
})
