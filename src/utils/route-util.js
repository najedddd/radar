import { createHistory } from 'history'
import { useRouterHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

let history = null

export function getHistory() {
    return history
}

export function initHistory(store) {
    history = syncHistoryWithStore(useRouterHistory(createHistory)({
        basename: window.ATLASSIAN.context
    }), store)
}

export function qualifyResourcePath(path) {
    return `/resource${path}`
}

export function unqualifyResourcePath(value) {
    const resourcePath = decodeURIComponent(value).replace(/.*\/resource/, '')

    if (resourcePath === '/') {
        return '' // This is the 'root' resource.
    }
    return resourcePath
}
