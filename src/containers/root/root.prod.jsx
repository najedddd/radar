import React from 'react'
import App from '../app.jsx'

const Root = ({ children, location }) => (<div>
    <App children={children} location={location} />
</div>)

Root.propTypes = {
    children: React.PropTypes.object,
    location: React.PropTypes.object
}

export default Root
