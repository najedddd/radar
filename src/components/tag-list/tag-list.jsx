import './tag-list.less'

import React from 'react'
import { Link } from 'react-router'
import Markdown from 'react-markdown'

import ExternalDocs from '../external-docs/external-docs.jsx'

export default class TagList extends React.Component {

    renderTags() {
        const { tags } = this.props
        if (!tags) {
            return null
        }
        const description = (value) => {
            if (value) {
                return <Markdown source={value} skipHtml />
            }
            return <span className="no-description">(No description)</span>
        }

        return tags.map((value) => <tr key={value.name}>
            <td key="tag-name"><Link to={`/search?q=tag:${value.name}`}>{value.name}</Link></td>
            <td key="tag-description">
                {description(value.description)}
                <span> </span>
                {ExternalDocs.fromOptionalObject(value.externalDocs)}
            </td>
        </tr>)
    }

    render() {
        return (<div>
            <h3>Tags</h3>
            <table className="aui resource-list-table">
                <tbody>
                    {this.renderTags()}
                </tbody>
            </table>
        </div>)
    }
}

TagList.propTypes = {
    tags: React.PropTypes.arrayOf(React.PropTypes.shape({
        name: React.PropTypes.string,
        description: React.PropTypes.string,
        externalDocs: React.PropTypes.shape(ExternalDocs.propTypes)
    }))
}
