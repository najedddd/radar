import React from 'react'

const oAuthFlows = {
    imlicit: 'implicit',
    password: 'password',
    application: 'application',
    accessCode: 'access code'
}

const OAuth = ({ flow, scopes, authorizationUrl, tokenUrl }) => {
    const scopesArr = []
    Object.keys(scopes).forEach(key => {
        scopesArr.push(<span><code>{key}</code> - {scopes[key]}</span>)
    })
    return (<div>
        <span>This API uses the <strong>{oAuthFlows[flow] || flow}</strong> flow.</span>
        <p>Authorization URL: <a href={authorizationUrl}>{authorizationUrl}</a></p>
        <p>Token URL: <a href={tokenUrl}>{tokenUrl}</a></p>
        <h5>Scopes</h5>
        <ul>
            {scopesArr.map((scope, i) => <li key={i}>{scope}</li>)}
        </ul>
    </div>)
}

OAuth.propTypes = {
    flow: React.PropTypes.oneOf(Object.keys(oAuthFlows)),
    scopes: React.PropTypes.object,
    authorizationUrl: React.PropTypes.string,
    tokenUrl: React.PropTypes.string
}

export default OAuth
