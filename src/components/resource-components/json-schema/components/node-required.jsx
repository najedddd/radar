import React from 'react'

const NodeRequired = ({ required }) => {
    if (!required) {
        return null
    }
    return <span className="node-required"></span>
}

NodeRequired.propTypes = {
    required: React.PropTypes.bool
}

export default NodeRequired
