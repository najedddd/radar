import './search.less'

import React from 'react'
import Autosuggest from 'react-autosuggest'

import { getHistory } from '../../utils/route-util'

export default class Search extends React.Component {

    onSearchChange(event, newValue) {
        const { location } = this.props

        if (newValue !== '') {
            if (location.query.q && location.query.q.endsWith(':') ||
                location.pathname.indexOf('/search') !== 0) {
                getHistory().push({
                    pathname: '/search',
                    query: { q: newValue }
                })
            } else {
                getHistory().replace({
                    pathname: '/search',
                    query: { q: newValue }
                })
            }
        } else {
            getHistory().push('/')
        }
    }

    getSuggestionValue(suggestion) {
        return suggestion.query
    }

    renderSuggestion(suggestion) {
        return (
            <div>
                <span className="title">{suggestion.title}</span>
                <span className="description">{suggestion.description}</span>
            </div>)
    }

    render() {
        const { location, suggestions } = this.props
        const inputProps = {
            placeholder: 'Search...',
            value: location.query.q || '',
            onChange: (event, { newValue }) => this.onSearchChange(event, newValue)
        }
        return (<Autosuggest
          suggestions={suggestions}
          shouldRenderSuggestions={(value) => value.length === 0}
          inputProps={inputProps}
          renderSuggestion={this.renderSuggestion}
          getSuggestionValue={this.getSuggestionValue}
        />)
    }
}

Search.propTypes = {
    location: React.PropTypes.object,
    suggestions: React.PropTypes.arrayOf(React.PropTypes.shape({
        query: React.PropTypes.string,
        title: React.PropTypes.string,
        description: React.PropTypes.string
    }))
}
