import { combineReducers } from 'redux'

import configureStore from './src/store/configure-store.prod'
import * as reducers from './src/reducers/all-reducers'
import { dataLoad } from './src/action-creators/data-action-creators'

export function mockStore(data) {
    const store = configureStore(combineReducers({ ...reducers }))
    store.dispatch(dataLoad(data))
    return store
}
